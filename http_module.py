import socket

def SimpleHTTP(ip, port, request):

    CRLF = "\r\n"

    request = [
        "GET %s HTTP/0.9" % request,
        "Host: %s:%s" % (ip, port),
        "Connection: Close",
        "",
        "",
    ]

    # Connect to the server
    s = socket.socket()
    s.settimeout(2)
    s.connect((ip, port))

    # Send an HTTP request
    s.send(CRLF.join(request).encode())

    # Get the response (in several parts, if necessary)
    response = ''
    buffer = s.recv(4096).decode()
    while buffer:
        response += buffer
        buffer = s.recv(4096).decode()

    # HTTP headers will be separated from the body by an empty line
    header_data, _, body = response.partition(CRLF + CRLF)

    return header_data
