import configparser
import logging
import sys

#Read config
configfile = configparser.ConfigParser()

#Check for config section
if configfile.read('config.ini') == []:
    print('[ERROR] Config file not found, check for config.ini')
    logging.critical('Config file not found, check for config.ini')
    sys.exit()

STATION = configfile.get('GENERAL','STATION')
TIMDELAY = configfile.getint('GENERAL','TIMDELAY')
capcode = configfile.get('GENERAL','CAPCODE')
PDTon = configfile.getboolean('PDT', 'ENABLED')
PDTCOM = configfile.get('PDT','COM')
SMSon = configfile.getboolean('SMS', 'ENABLED')
SMSCOM = configfile.get('SMS','COM')
ACKon = configfile.getboolean('SMS', 'SMSACK')
ACKnum = configfile.get('SMS','ACKNUM')
SMSwhitelist = configfile.get('SMS','WHITELIST').split(',')
Relayon = configfile.getboolean('RELAY','ENABLED')
RelayIP = configfile.get('RELAY','IP')
RelayPort = configfile.getint('RELAY','PORT')
Relay1 = configfile.getboolean('RELAY','RELAY1')
Relay2 = configfile.getboolean('RELAY','RELAY2')
Relay3 = configfile.getboolean('RELAY','RELAY3')
Relay4 = configfile.getboolean('RELAY','RELAY4')
