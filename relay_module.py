########################################################
### This module reads and controls a WebRelay Device ###
########################################################

import re
import logging
from http_module import SimpleHTTP

def GetRelayState(deviceip, deviceport):
    logging.debug('Getting relay states from %s:%s')
    try:
        response = urllib.request.urlopen('http://%s:%s/state.xml' % (deviceip, deviceport), timeout = 5)
    except URLError:
        logging.exception('Unable to read WebRelay')
    html = response.read()
    logging.debug('Received response %s' % html)
    try:
        state = re.search('<relaystate>(\d)<\/relaystate>', html).group(1)
        logging.debug('State found %s' % state)
        return state
    except AttributeError:
        logging.debug('State not found')
    print('[RELAY] Can not communicate to WebRelay')

def SetRelayState(deviceip, deviceport, relay, value):
    try:
        SimpleHTTP(deviceip, deviceport, '/state.xml?relay%sState=%s' % (relay, value))
        logging.debug('Setting Relay @ %s:%s %s to State %s' %  (deviceip, deviceport, relay, value))
    except:
        logging.exception('WebRelay Relay Failed')
