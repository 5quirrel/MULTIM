#===========================================
#==== MULTIM PDT Serial and SMS to TIMS ====
#===========================================

from collections import deque
import threading
import re
import config
import logging
from logging.config import fileConfig
import os
import time
import datetime
import serial
from sms_module import SMSProcess, SMSSend
from pdt_module import PDTProcess
from relay_module import GetRelayState, SetRelayState

def GetINC(message):
    try:
        incident = re.search('(INC\d+) ',message).group(1)
        logging.debug('Found INC : %s in message', incident)
        return incident
    except AttributeError:
        logging.debug('No INC found in message')
        return False

def GetINCTime(message):
    logging.debug('Searching %s for time' % message)
    try: 
        time = re.search('MFS: \*CFSRES (INC\d+ \d+/\d+/\d+ \d+:\d+)',message).group(1)
        logging.debug('Found time %s' % time)
        return time
    except AttributeError:
        logging.debug('No INC time found in message')
        return False

def StationRelay():
    print('[RELAY] Firing Relay')
    logging.info('Triggering station relay')
    try:
        if Relay1 == True:
            SetRelayState(RelayIP, RelayPort, '1', '2')
        if Relay2 == True:
            SetRelayState(RelayIP, RelayPort, '2', '2')
        if Relay3 == True:
            SetRelayState(RelayIP, RelayPort, '3', '2')
        if Relay4 == True:
            SetRelayState(RelayIP, RelayPort, '4', '2')
        logging.info('Station relay triggered')
    except:
        logging.exception('Failed to trigger relay!')

def SendTIM(message):
    try:
        print('===============================================')
        os.system('TurnOutClient.exe %s "%s"' % (capcode, message))
        print('===============================================')
        logging.info('Sent %s to TIM' % message)
        recvlist.append(msgtime)
        logging.debug('Added %s to sent message list' % msgtime)
        INC = GetINC(message)
        if ACKon == True and INC != False:
            logging.info('Sending ACK message for %s' % INC)
            print(('[MSG] Sending ACK for %s' % INC))
            ackmsg = 'TURNOUT: Station %s has received %s' % (STATION, INC)
            ackqueue.append(ackmsg)
    except:
        logging.exception('Failed to send %s to TIM' % message)
        print('[MSG] Send to TIM failed. Check TIM is running!')
    time.sleep(TIMDELAY)

def SMSthread():
    while True:
        smsrecv = SMSProcess(cellular)
        if smsrecv != False:
            logging.info('SMS Recieved %s' % smsrecv)
            msgqueue.extend(smsrecv)
            print('[SMS] SMS Received')
        if ACKon:
            try:
                while ackqueue:
                    outmsg = ackqueue.popleft()
                    logging.debug('Sending "%s" to %s' % (outmsg, ACKnum))
                    SMSSend(cellular, outmsg, ACKnum)
            except IndexError:
                pass
            except:
                print('[SMS] ACK Failed')
                logging.exception('Failed to send ACK')

def PDTthread():        
    while True:
        pdtrecv = PDTProcess(pdt)
        if pdtrecv != False:
            logging.info('PDT Recieved %s' % pdtrecv)
            msgqueue.append(pdtrecv)
            print('[PDT] Data Received')
        time.sleep(0.1)

def FailQuit():
    input("[Hit Enter to Quit!]")
    logging.info('Quitting!')
    quit()

#Start logging based on logging.conf
fileConfig('logging.conf')
logger = logging.getLogger()

#Version Number
ver = 'v1.0 Beta'

logging.info('MULTIM %s Start' % ver)
print((format('', '=^50')))
print(('+%s+' % format('MULTIM %s' % ver, '^48')))
print(('+%s+' % format('MULtiple-Source Turnout Information Manager', '^48')))
print(('+%s+' % format('message forwarder', '^48')))
print((format('', '=^50')))

#Rename variables
STATION = config.STATION
TIMDELAY = config.TIMDELAY
capcode = config.capcode
PDTon = config.PDTon
PDTCOM = config.PDTCOM
SMSon = config.SMSon
SMSCOM = config.SMSCOM
ACKon = config.ACKon
ACKnum = config.ACKnum
Relayon = config.Relayon
RelayIP = config.RelayIP
RelayPort = config.RelayPort
Relay1 = config.Relay1
Relay2 = config.Relay2
Relay3 = config.Relay3
Relay4 = config.Relay4

#START PDT

if PDTon:
    try:
        pdt = serial.Serial(PDTCOM, 9600, timeout=1)
        logging.info('Configured PDT Serial on %s' % PDTCOM)
        print('[PDT] Input Enabled')
    except:
        logging.exception('Failed to configure PDT on %s! Check serial ports in config.ini!' % PDTCOM)
        print('[PDT] Serial Setup Failed!')
        FailQuit()
    try:
        logging.info('PDT Thread Starting')
        print('[PDT] PDT Thread Starting')
        t_pdt = threading.Thread(target=PDTthread)
        t_pdt.daemon = True
        t_pdt.start()
    except:
        logging.exception('Unable to start PDT thread')
else:
    print('[PDT] Input Disabled')
    logging.info('PDT Disabled by config')

#START SMS

if SMSon:
    try:
        cellular = serial.Serial(SMSCOM, 115200, timeout=1)
        logging.info('Configured SMS Serial on %s' % SMSCOM)
        print('[SMS] Input Enabled')
    except:
        logging.exception('Failed to configure SMS cellular on %s! Check serial ports in config.ini!' % SMSCOM)
        print('[SMS] Serial Setup Failed!')
        FailQuit()
    try:
        logging.info('SMS Thread Starting')
        print('[SMS] SMS Thread Starting')
        t_sms = threading.Thread(target=SMSthread)
        t_sms.daemon = True
        t_sms.start()
    except:
        logging.exception('Unable to start SMS thread')
    if ACKon:
        logging.info('SMS acknowledgements are enabled')
        print('[SMS] SMS acknowledgements are enabled')
        #Ack outgoing queue
        ackqueue = deque([])
        logging.info('Created Ack Queue')
else:
    print('[SMS] Input Disabled')
    logging.info('SMS Disabled by config')

#Message processing queue
msgqueue = deque([])
logging.info('Created Message Queue')

#Message date/time list
recvlist = []
             
logging.info('Started Processing Messages')
print('[MSG] Message Monitoring Begins')

#Queue Monitoring and Processing Loop

while True:
    try:
        while msgqueue:
            logging.debug('Message received in queue')
            msg = msgqueue.popleft()
            logging.debug('Processing %s from queue' % msg)
            msgtime = GetINCTime(msg)
            if msgtime != False:
                if msgtime in recvlist:
                    logging.debug('Discaring Message "%s" with timestamp %s as duplicate' % (msg, msgtime))
                    break
                if Relayon:
                    StationRelay()
                logging.debug('Message "%s" being sent to TIM function' % msg)
                SendTIM(msg)
            else:
                #Skip and send
                if Relayon:
                    StationRelay()
                logging.debug('Message "%s" does not match SACAD format, automatically sending to TIM' % msg)
                SendTIM(msg)
    except IndexError:
        pass
    #Pause
    time.sleep(0.1)

