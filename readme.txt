===============================================
+                 MULTIM v1.0                 +
+ MULtiple-Source Turnout Information Manager +
+              message forwarder              +
===============================================

1. Required Files

The following files must all be located in the same folder.

MULTIM.exe		- Main program executable
config.ini		- Configuration file
TurnOutClient.exe	- TIMS interface application
logging.conf		- Logging configuration file

2. config.ini Explained

[GENERAL]
STATION	= 		- Test
TIMDELAY = 		- Delay between MULTIM sending messages to TIM
CAPCODE = 		- Specify capcode to be sent to TIM with messages

[PDT]
ENABLED = True/False	- PDT enabled
COM = COM#		- Specify COM port for PDT

[SMS]
ENABLED = True/False	- SMS enabled
COM = COM#		- Specify COM port for cellular modem
SMSACK = True/False	- Send ACK messages
ACKNUM = +61#		- ACK message destination
WHITELIST = +61#,+61#	- Whitelist of SMS senders

[RELAY]
ENABLED = True/False	- Enable Relays
IP = #.#.#.#		- Relay IP address
PORT = ##		- Relay Port Number
RELAY1 = True/False	- Enable Relay
RELAY2 = True/False	- Enable Relay
RELAY3 = True/False	- Enable Relay
RELAY4 = True/False	- Enable Relay