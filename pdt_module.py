##################################################################################
### This module reads PDT messages and forwards them to the main MULTIM module ###
##################################################################################

import serial
import re
import logging
import time


def PDTProcess(pdt):
    pdtrx = []
    while pdt.in_waiting != 0:
        logging.debug('PDT received data')
        pdtrxline = pdt.readline().decode()
        logging.debug('Read line "%s"' % pdtrxline)
        
        try:
            pdtrx = re.search('\d+ A (.*)',pdtrxline).group(1)
            return pdtrx
        
        except AttributeError:
            logging.debug('Search string not found in "%s"' % pdtrxline)
            continue
            
    else:
        return False
