####################################################################################################
### This module reads and compiles multipart SMS messages returning the last SMS it put together ###
####################################################################################################

import config
import serial
import re
import time
import datetime
import logging
from messaging.sms import SmsDeliver, SmsSubmit

def SendCommand(command, modem):
    modem.write(command.encode())
    modem.write('\r'.encode())
    time.sleep(0.2)

def SMSNotifyRX():
    smsrxqty = 0
    if modem.in_waiting != 0:
        #Check for notify messages and add storage positions to list
        smsrxnotify = re.findall('CMTI: "ME",(\d+)',modem.read(modem.in_waiting).decode())
        smsrxqty = len(smsrxnotify)
    return smsrxqty

def SMSinmem(modem):
    #Set to PDU mode
    SendCommand('AT+CMGF=0', modem)
    #Check for all messages stored and add storage positions to list to be returned
    SendCommand('AT+CMGL=4', modem)
    #Make a list
    smsinmem = []
    memreadline = modem.readline().decode()
    while memreadline != '':
        try:
            smsinmem.append(re.search('\+CMGL: (\d+),',memreadline).group(1))
        except AttributeError:
            pass
        memreadline = modem.readline().decode()
    return smsinmem

def SMSSend(modem, message, number):
    #Enter text mode
    SendCommand('AT+CMGF=1', modem)
    modem.write('AT+CMGS="%s"\r'.encode() % number)
    time.sleep(0.2)
    modem.write(('%s\x1A' % message).encode())
    time.sleep(0.2)
    #Revert to PDU
    SendCommand('AT+CMGF=0', modem)

def SMSProcess(modem):
    #Create SMS process and storage dictionaries
    smsparts = {}
    smsmemloc = {}
    smsseqcount = {}
    if SMSinmem(modem) != []:
        logging.debug('Message found in memory, waiting briefly for stragglers')
        #Wait time
        time.sleep(15)
        processlocations = SMSinmem(modem)
        logging.debug('SMSinmem returned list %s processing now' % processlocations)
        for loc in processlocations:
            #Send message to retrive message
            logging.debug('Reading SMS in memory location %s' % loc)
            SendCommand('AT+CMGR='+loc, modem)
            #Retrive and store for processing
            pdu = re.search('CMGR: \d+,,\d+\r\n(\w*)\r\n\r\nOK',modem.read(modem.in_waiting).decode()).group(1)
            #Process into SMS class
            sms = SmsDeliver(pdu)

            #Check if sender is on whitelist
            logging.debug('Checking whitelist')
            if sms.number not in config.SMSwhitelist:
                logging.warning('Number %s not in whitelist, dropping this message!' % sms.number)
                SendCommand('AT+CMGD='+loc, modem)
                continue
            
            #Check for single part message and process if so
            if 'cnt' not in sms.data:
                logging.debug('Did not find count in UDH of SMS (not a concatenated SMS)')
                logging.debug('Working with SMS from : %s at %s' % (sms.number, str(sms.date)))
                check = 0
                while check <= 5:
                    checktag = sms.number + str(sms.date - datetime.timedelta(seconds=check))
                    logging.debug('Looking for %s in SMS tags' % checktag)
                    if checktag in smsparts:
                        logging.debug('Found exsisting tag within range at %s sec old - (%s) %s appending %s' % (check, checktag, smsparts[checktag][1], sms.text))
                        smsparts[checktag][1] += ' '+sms.text
                        smsmemloc[checktag].append(loc)
                        break
                    check += 1
                else:
                    smstag = sms.number + str(sms.date)
                    logging.debug('Did not find matching tag within range. Making new tag for (%s) %s' % (smstag, sms.text))                    
                    smsparts[smstag] = {}
                    smsparts[smstag][1] = sms.text
                    smsseqcount[smstag] = 1
                    smsmemloc[smstag] = []
                    smsmemloc[smstag].append(loc)
            else:
                #Continue for Multi-Part SMS
                logging.debug('Found CONCAT SMS')
                #Make a dictionary key is one doesn't exsist
                if sms.udh.concat.ref not in smsparts:
                    smsparts[sms.udh.concat.ref] = {}
                if sms.udh.concat.ref not in smsmemloc:
                    smsmemloc[sms.udh.concat.ref] = []
                #Store into processing and sequence count dictionary
                smsparts[sms.udh.concat.ref][sms.udh.concat.seq] = sms.text
                smsseqcount[sms.udh.concat.ref] = sms.udh.concat.cnt
                smsmemloc[sms.udh.concat.ref].append(loc)
            logging.debug('SMS memory processing complete, waiting before next check')
        
    #Process into complete messages if all parts received
    smsrx = []
    for ref in list(smsparts):
        #Reset compiledsms
        compiledsms = ''
        #Check if all parts are rx and combine is they are
        if len(smsparts[ref]) == smsseqcount[ref]:
            #Process each sequence into a long string
            logging.debug('Processing SMS %s' % ref)
            for seq in sorted(smsparts[ref]):
                #add to string
                compiledsms += smsparts[ref][seq]
                logging.debug('Compiling SMS %s' % compiledsms)
            smsrx.append(compiledsms)
            #Delete sequence count
            del smsseqcount[ref]
            #Delete parts in dictionary
            del smsparts[ref]
            #Delete SMS in modem
            for i in smsmemloc[ref]:
                #Delete the message
                logging.debug('Deleting SMS message in memory position %s' % i)
                SendCommand('AT+CMGD='+i, modem)
            del smsmemloc[ref]
            
    #Return compiled SMS list or False
    if smsrx != []:
        logging.debug('Returning SMS list of %s' % smsrx)
        return smsrx
    else:
        return False


